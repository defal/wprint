# wprint

a quick and dirty shell script for taking screenshots on wayland and placing saving them onto your disk or your clipboard.

# dependencies

- grim
- slurp
- wl-clipboard 
- imv 

# TODO

remove from list when done

- [ ] Fullscreen screenshots
- [ ] Better image preview when selecting an area
- [ ] Format option (remove force option)
- [ ] Mutiple monitor setup

# install

Just run `./wprint` and it should run the command just fine.
If you want to run it from a compositor or as a macro, you should probably move the script to somewhere in your `$PATH` like `/usr/bin/` or `/usr/local/bin`

To use *wprint* with dwl, you can use my [patched version](https://codeberg.org/defal/defalwl/)
or you can patch it manually by install it somewhere in your `$PATH` and then add a pointer command to your `config.h`:
```
/* commands */
static const char *termcmd[] = { "foot", NULL };
static const char *menucmd[] = { "bemenu-run", NULL };
static const char *print[] = { "wprint", NULL };
```

also add this line inside your `keys[]`
like so:
```
static const Key keys[] = {
    ...your stuff here

    { 0,			    XKB_KEY_Print,	spawn,		{.v = print } },
    ...
}
```

# options

```
wprint - a simple bash script to take printscreens on wayland.

Syntax: wprint [-h|-f]
    options:
    -h			|	print this help.
    -v			|	verbose mode.
    -f 			| 	Force to take a print even if unable.
    -c 			| 	Copy output to clipboard, not saving the output.
    -o </some/where/>	|	Path to save the output. Use with -c to save it
```

# credits

- Zircon for the original version of this script, which helped me alot.
- All of the creators of such amazing software used in this project.
